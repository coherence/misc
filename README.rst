======================================
Coherence Miscellaneous Branches
======================================

This repository holds miscellaneous stuff from the original Coherence
Subversion repository at coherence.beebits.net, which did not look
important enough to get a git repository by its own.

The branches from the original Subversion repository have been put
into the following branches here:

* talks: slides from conference

* UPnP-Test: Another test-suite by micxer. Relation to UPnT is unknown.

* maemo: Maemo .install files

* SunriseAndSunset: calculates sunrise or sunset for a given time and
  place.

* test2: some tests, details got lost in history

..
  Local Variables:
  mode: rst
  ispell-local-dictionary: "american"
  End:
